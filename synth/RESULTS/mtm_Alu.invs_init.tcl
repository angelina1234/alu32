#####################################################################
#
# Init setup file
# Created by Genus(TM) Synthesis Solution on 09/05/2019 21:22:59
#
#####################################################################


read_mmmc /home/student/akowalik/PPCU_VLSI/alu/alu_projekt_vlsi/synth/RESULTS/mtm_Alu.mmmc.tcl

read_physical -lef {/cad/dk/umc180/SUS/SUSLIB_UCL_tech.lef /cad/dk/umc180/SUS/SUSLIB_UCL.lef}

read_netlist /home/student/akowalik/PPCU_VLSI/alu/alu_projekt_vlsi/synth/RESULTS/mtm_Alu.v

init_design
