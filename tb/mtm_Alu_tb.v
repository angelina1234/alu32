
/******************************************************************************
 * (C) Copyright 2019 AGH UST All Rights Reserved
 *
 * MODULE:    mtm_Alu tb
 * PROJECT:   PPCU_VLSI
 * AUTHORS:	  Angelina Kowalik
 * DATE:	  05.09.2019
 * ------------------------------------------------------------------------------
 * This module (TB) provides test patterns for the ALU, reads data from the ALU and 
 * verifies if the operation result is correct.
 * 
 * The TB must include:
 * - task send_byte to send a CMD or CTL command to the ALU
 * - task send_calculation_data that will send 9 bytes to the ALU for given
 *   operands and operation
 * - procedural block for capturing the input data from the ALU
 * - task compare to compare the result from the ALU and the expected data.
 * 
 * The test vectors must provide at least:
 * - sending max (0xFFFF) and min (0) data with all the ALU operations (AND OR, ADD,SUB)
 * - sending 1000 random valid data
 * - sending invalid data (wrong number of DATA packets before CTL packet)
 * - sending data with CRC error
 * 
 * The testbench should print final PASS/FAIL text information.
 */

module mtm_Alu_tb (
    output reg clk,
    output reg rst_n,
    output reg sin,
    input wire sout,
    input [8:0] data_out
) ;

	localparam  WIDTH = 8,
				WAIT_BYTE_TICKS = 90,
				AND = 3'b000,
				OR = 3'b001,
				ADD = 3'b100,
				SUB = 3'b101;	

	reg [10:0] byte_to_send;
	reg [7:0] bit_counter;
	reg [31:0] A, B;
	reg [2:0] OP;
	reg [3:0] CRC;
	reg [9:0] passed_tests;
	reg [9:0] failed_tests;

	always begin
		#5 clk = 0;
		#5 clk = 1;
	end

	task send_byte; 
		input reg [10:0] byte_to_send;
		input reg [4:0] bit_counter;
		output reg sin;
		
		begin
			sin = byte_to_send[10-bit_counter];
			#10; //$display("bit_counter: %h\n", bit_counter);
		end
	endtask
	
	task send_calculation_data;
		input reg [31:0] A;
		input reg [31:0] B;
		input reg [2:0] OP; 
		input reg [3:0] CRC;
		input reg [7:0] bit_counter;
		output reg sin;
		reg [5:0] byte_counter;
		reg [10:0] byte_to_send;
		
		begin
		sin = 1;
		byte_counter = bit_counter/12;
		if(byte_counter < 4)
			byte_to_send = {2'b00, B[31-(byte_counter*8)-:8], 1'b1};
		else if(byte_counter < 8)
			byte_to_send = {2'b00, A[31-((byte_counter-4)*8)-:8], 1'b1};
		else
			byte_to_send = {2'b01, 1'b0, OP, CRC, 1'b1};
			
		if(bit_counter%12 != 0)
			send_byte(byte_to_send, (bit_counter%12) - 1, sin);
		//$display("bit_countermod12: %h\tsin: %b\n", bit_counter%12, sin);
		end
	endtask
	
	reg accuisition_on_f = 0;
	reg data_f = 0;
	reg ctl_f = 0;
	reg [4:0] byte_counter = 0;
	reg [4:0] bit_counter_r = 0;
	reg [31:0] C;
	reg [7:0] CTL;
	reg [31:0] C_tb;
	reg [7:0] CTL_tb;
	
	always @(posedge clk) begin
		if(!accuisition_on_f && !sout)
			accuisition_on_f = 1;
		else if(accuisition_on_f) begin
			if(!sout && !data_f && !ctl_f) begin
				data_f = 1;
				ctl_f = 0;
				byte_counter = byte_counter + 1;
			end
			else if(sout && !data_f && !ctl_f) begin
				ctl_f = 1;
				data_f = 0;
				byte_counter = byte_counter + 1;
			end
			else if(data_f && bit_counter_r < 8) begin	
				C[31-(bit_counter_r + (byte_counter-1)*8)] = sout;
				bit_counter_r = bit_counter_r + 1;
				//$display("C: %b\n", C);
			end
			else if(ctl_f && bit_counter_r < 8) begin
				CTL[7-bit_counter_r] = sout;
				bit_counter_r = bit_counter_r + 1;
				byte_counter = 0;
				//$display("CTL: %b\n",CTL);
			end
			else if(bit_counter_r >= 8 && sout) begin
				bit_counter_r = 0;
				data_f = 0;
				ctl_f = 0;
				accuisition_on_f = 0;
				if(byte_counter >= 5) begin
					byte_counter = 0;
				end
			end					
		end
	end
	//reg [5:0] counter;
	
	task calculate_crc_4bit;
		parameter data_size = 68;
		parameter crc_size = 4;
		input [data_size-1 : 0] data;
		output reg [crc_size-1:0] CRC;
		
		reg [data_size + crc_size - 1:0] registers;
		reg [6:0] counter_crc;
		reg [data_size + crc_size - 1:0] poly_with_zeros;
		reg [crc_size:0] poly;
		
		begin
			case(crc_size)
				3: poly = 4'h15;
				4: poly = 5'h19;
			endcase
			registers = {data, {crc_size{1'b0}}};
			poly_with_zeros = {poly, {data_size-1{1'b0}}};
			counter_crc = 0;
			//$display("registers: %h", registers);
			while(counter_crc < data_size) begin
				while(!(registers[data_size + crc_size - 1 - counter_crc]))
					counter_crc = counter_crc + 1;	
				registers = registers ^ (poly_with_zeros >> counter_crc);
				counter_crc = counter_crc + 1;
			end		
			CRC = registers[crc_size-1:0];
			$display("CRC: %h, registers: %h", CRC, registers);
		end		
	endtask
	
	task calculate_crc_3bit;
		parameter data_size = 37;
		parameter crc_size = 3;
		input [data_size-1 : 0] data;
		output reg [crc_size-1:0] CRC;
		
		reg [data_size + crc_size - 1:0] registers;
		reg [6:0] counter_crc;
		reg [data_size + crc_size - 1:0] poly_with_zeros;
		reg [crc_size:0] poly;
		
		begin
			case(crc_size)
				3: poly = 4'h15;
				4: poly = 5'h19;
			endcase
			registers = {data, {crc_size{1'b0}}};
			poly_with_zeros = {poly, {data_size-1{1'b0}}};
			counter_crc = 0;
			//$display("registers: %h", registers);
			while(counter_crc < data_size) begin
				while(!(registers[data_size + crc_size - 1 - counter_crc]))
					counter_crc = counter_crc + 1;	
				registers = registers ^ (poly_with_zeros >> counter_crc);
				counter_crc = counter_crc + 1;
			end		
			CRC = registers[crc_size-1:0];
			//$display("CRC: %h, registers: %h", CRC, registers);
		end		
	endtask

	task calculate_outputs;
		input [31:0] A;
		input [31:0] B;
		input [2:0] OP;
		input [3:0] CRC;
		output [7:0] CTL;
		output [31:0] C;
		
		reg carry_f, overflow_f, zero_f, negative_f;
		reg [2:0] CRC_out;
		reg [2:0] CRC_check;
		
		begin
		case(OP)
			AND:	C = A & B;
			OR:		C = A | B; 
			SUB:	C = A - B;
			ADD:	C = A + B;
			default: C = 0;
		endcase
		
		zero_f = (C == 0);
		negative_f = C[31];
		carry_f = (((OP == ADD) && ((C < A) || (C < B))) || ((OP == SUB) && (A < C)));
		overflow_f = (((OP == ADD) && !(A[31]^B[31]) && (A[31]^C[31])) || ((OP == SUB) && !(A[31]^C[31]) && (B[31]^C[31])));
		CRC_out = CRC3_D37({C, 1'b0, carry_f, overflow_f, zero_f, negative_f}, 3'b000);
		calculate_crc_3bit({C, 1'b0, carry_f, overflow_f, zero_f, negative_f}, CRC_check);
		$display("CHECK CRC: %h\n", CRC_check);
		assign CTL = {1'b0, carry_f, overflow_f, zero_f, negative_f, CRC_out};
		
		end
		
	endtask
	
	task compare_results;
		input [31:0] C, C_tb;
		input [7:0] CTL, CTL_tb;
		
		begin
			if(C == C_tb && CTL == CTL_tb) begin
				passed_tests = passed_tests + 1;
				$display("PASS");
				end
			else begin
				failed_tests = failed_tests + 1;
				$display("FAIL");
				end
		end
	endtask
	
	reg [2:0] op_cnt;
	reg [9:0] cnt;
	reg [5:0] counter;
	
	function [3:0] CRC4_D68;

		input [67:0] Data;
		input [3:0] crc;
		reg [67:0] d;
		reg [3:0] c;
		reg [3:0] newcrc;
	  begin
		d = Data;
		c = crc;

		newcrc[0] = d[67] ^ d[65] ^ d[63] ^ d[62] ^ d[61] ^ d[60] ^ d[56] ^ d[53] ^ d[52] ^ d[50] ^ d[48] ^ d[47] ^ d[46] ^ d[45] ^ d[41] ^ d[38] ^ d[37] ^ d[35] ^ d[33] ^ d[32] ^ d[31] ^ d[30] ^ d[26] ^ d[23] ^ d[22] ^ d[20] ^ d[18] ^ d[17] ^ d[16] ^ d[15] ^ d[11] ^ d[8] ^ d[7] ^ d[5] ^ d[3] ^ d[2] ^ d[1] ^ d[0] ^ c[1] ^ c[3];
		newcrc[1] = d[66] ^ d[64] ^ d[63] ^ d[62] ^ d[61] ^ d[57] ^ d[54] ^ d[53] ^ d[51] ^ d[49] ^ d[48] ^ d[47] ^ d[46] ^ d[42] ^ d[39] ^ d[38] ^ d[36] ^ d[34] ^ d[33] ^ d[32] ^ d[31] ^ d[27] ^ d[24] ^ d[23] ^ d[21] ^ d[19] ^ d[18] ^ d[17] ^ d[16] ^ d[12] ^ d[9] ^ d[8] ^ d[6] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ c[0] ^ c[2];
		newcrc[2] = d[67] ^ d[65] ^ d[64] ^ d[63] ^ d[62] ^ d[58] ^ d[55] ^ d[54] ^ d[52] ^ d[50] ^ d[49] ^ d[48] ^ d[47] ^ d[43] ^ d[40] ^ d[39] ^ d[37] ^ d[35] ^ d[34] ^ d[33] ^ d[32] ^ d[28] ^ d[25] ^ d[24] ^ d[22] ^ d[20] ^ d[19] ^ d[18] ^ d[17] ^ d[13] ^ d[10] ^ d[9] ^ d[7] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ c[0] ^ c[1] ^ c[3];
		newcrc[3] = d[67] ^ d[66] ^ d[64] ^ d[62] ^ d[61] ^ d[60] ^ d[59] ^ d[55] ^ d[52] ^ d[51] ^ d[49] ^ d[47] ^ d[46] ^ d[45] ^ d[44] ^ d[40] ^ d[37] ^ d[36] ^ d[34] ^ d[32] ^ d[31] ^ d[30] ^ d[29] ^ d[25] ^ d[22] ^ d[21] ^ d[19] ^ d[17] ^ d[16] ^ d[15] ^ d[14] ^ d[10] ^ d[7] ^ d[6] ^ d[4] ^ d[2] ^ d[1] ^ d[0] ^ c[0] ^ c[2] ^ c[3];
		CRC4_D68 = newcrc;
	  end
	  endfunction
  
	function [2:0] CRC3_D37;

		input [36:0] Data;
		input [2:0] crc;
		reg [36:0] d;
		reg [2:0] c;
		reg [2:0] newcrc;
	  begin
		d = Data;
		c = crc;

		newcrc[0] = d[36] ^ d[35] ^ d[32] ^ d[30] ^ d[29] ^ d[28] ^ d[25] ^ d[23] ^ d[22] ^ d[21] ^ d[18] ^ d[16] ^ d[15] ^ d[14] ^ d[11] ^ d[9] ^ d[8] ^ d[7] ^ d[4] ^ d[2] ^ d[1] ^ d[0] ^ c[1] ^ c[2];
		newcrc[1] = d[36] ^ d[33] ^ d[31] ^ d[30] ^ d[29] ^ d[26] ^ d[24] ^ d[23] ^ d[22] ^ d[19] ^ d[17] ^ d[16] ^ d[15] ^ d[12] ^ d[10] ^ d[9] ^ d[8] ^ d[5] ^ d[3] ^ d[2] ^ d[1] ^ c[2];
		newcrc[2] = d[36] ^ d[35] ^ d[34] ^ d[31] ^ d[29] ^ d[28] ^ d[27] ^ d[24] ^ d[22] ^ d[21] ^ d[20] ^ d[17] ^ d[15] ^ d[14] ^ d[13] ^ d[10] ^ d[8] ^ d[7] ^ d[6] ^ d[3] ^ d[1] ^ d[0] ^ c[0] ^ c[1] ^ c[2];
		CRC3_D37 = newcrc;
	  end
	endfunction

	task display_test_results;
		input [31:0] A, B;
		input [2:0] OP;
		input [3:0] CRC;
		input [31:0] C_tb, C;
		input [7:0] CTL_tb, CTL;
		begin
		$display("\nDATA FROM TESTBENCH");
		$display("A: %h\t B: %h\t, OP: %h\t CRC: %h", A, B, OP, CRC);
		$display("C_tb: %h\t CTL_tb: %h", C_tb, CTL_tb);
		$display("FLAGS - Carry: %h, Overflow: %h, Zero: %h, Negative: %h, CRC_C: %h", CTL_tb[6], CTL_tb[5], CTL_tb[4], CTL_tb[3], CTL_tb[2:0]);
		$display("\nDATA FROM ALU");
		#2500 $display("C: %h\t CTL: %h", C, CTL);
		$display("FLAGS from ALU - Carry: %h, Overflow: %h, Zero: %h, Negative: %h, CRC_C: %h", CTL[6], CTL[5], CTL[4], CTL[3], CTL[2:0]);
		end
	endtask  
	
	initial begin
		rst_n = 0;
		#40;
		rst_n = 1;
		sin = 1;
		passed_tests = 0;
		failed_tests = 0;
		#50;
//---------------------------------------TEST1-----------------------------------------------------
		$display("TEST1");	
		$display("START TEST\n");	
		A = 32'h0a0a0a0a;
		B = 32'haaaaaaaa;
		OP = AND;
		CRC = CRC4_D68({B, A, 1'b0, OP}, 4'h0);
		
		//calculate_crc_4bit({B, A, 1'b0, OP},CRC);
		#101;
		for(bit_counter = 0; bit_counter <= 10*11; bit_counter = bit_counter + 1)  begin
			send_calculation_data(A, B, OP, CRC, bit_counter, sin);	
		end
		sin = 1;
		calculate_outputs(A,B,OP,CRC,CTL_tb,C_tb);
		
		#2500 display_test_results(A,B,OP,CRC,C_tb,C,CTL_tb,CTL);

		compare_results(C, C_tb, CTL, CTL_tb);
		$display("END TEST\n\n\n");
		#500;
		
// ---------------------------SEND-WRONG-DATA-------------------------------------------------------		
		$display("WRONG DATA FRAMES");
		byte_to_send = 11'b00101010101;
		for(counter = 0; counter < 9; counter = counter + 1) begin
			for(bit_counter = 0; bit_counter <= 10; bit_counter = bit_counter +1)
				send_byte(byte_to_send, bit_counter, sin);
		end
			for(bit_counter = 0; bit_counter <= 10; bit_counter = bit_counter +1)
				send_byte(11'b11010101011, bit_counter, sin);
		C=0;
		#2500 $display("C: %h\t CTL: %h", C, CTL);
		compare_results(C, 32'h00000000, CTL, 8'b11001001);
		
		byte_to_send = 11'b00001011101;
		for(counter = 0; counter < 7; counter = counter + 1) begin
			for(bit_counter = 0; bit_counter <= 10; bit_counter = bit_counter +1)
				send_byte(byte_to_send, bit_counter, sin);
		end
		for(bit_counter = 0; bit_counter <= 10; bit_counter = bit_counter +1)
			send_byte(11'b11010101011, bit_counter, sin);
		C=0;
		#2500 $display("C: %h\t CTL: %h", C, CTL);
		compare_results(C, 32'h00000000, CTL, 8'b11001001);

		
		$display("\nWRONG CRC");
		A = 32'h10000ff0;
		B = 32'h10aa0000;
		OP = ADD;
		
	
		CRC = CRC4_D68({B, A, 1'b0, OP}, 4'h0);
		CRC = CRC + 2;
		
		#100;
		for(bit_counter = 0; bit_counter <= 10*11; bit_counter = bit_counter + 1)  begin
			send_calculation_data(A, B, OP, CRC, bit_counter, sin);	
		end
		sin = 1;
		
		C=0;
		#2500 $display("C: %h\t CTL: %h", C, CTL);
		compare_results(C, 32'h00000000, CTL, 8'b10100101);
// -------------------------OVERFLOW------------------------------------------------------------------
		$display("TEST2");	
		$display("START TEST\n");
		
		A = 32'h80000000;
		B = 32'h80000000;
		OP = ADD;
		CRC = CRC4_D68({B, A, 1'b0, OP}, 4'h0);

		#100;
		for(bit_counter = 0; bit_counter <= 10*11; bit_counter = bit_counter + 1)  begin
			send_calculation_data(A, B, OP, CRC, bit_counter, sin);	
		end
		sin = 1;
		calculate_outputs(A,B,OP,CRC,CTL_tb,C_tb);
		
		#2500 display_test_results(A,B,OP,CRC,C_tb,C,CTL_tb,CTL);
		
		compare_results(C, C_tb, CTL, CTL_tb);
		$display("\nEND TEST\n\n\n");
		#500;
// ----------------------------------------------------------------------------------------------
		
		for(op_cnt = 0; op_cnt < 4; op_cnt = op_cnt + 1) begin
		$display("start_test");
			case(op_cnt)
				0: OP = AND;
				1: OP = OR;
				2: OP = ADD;
				3: OP = SUB;
			endcase
			
			A = 32'h00000000;
			B = 32'h00000000;
			CRC = CRC4_D68({B, A, 1'b0, OP}, 4'h0);

			#100;
			for(bit_counter = 0; bit_counter <= 10*11; bit_counter = bit_counter + 1)  begin
				send_calculation_data(A, B, OP, CRC, bit_counter, sin);	
			end
			sin = 1;
			calculate_outputs(A,B,OP,CRC,CTL_tb,C_tb);
			
			#2500 display_test_results(A,B,OP,CRC,C_tb,C,CTL_tb,CTL);
			
			compare_results(C, C_tb, CTL, CTL_tb);
			$display("\nEND TEST\n\n\n");
			#500;
		end
//----------------------------------------------------------------------------------------------
		for(op_cnt = 0; op_cnt < 4; op_cnt = op_cnt + 1) begin
		$display("start_test");
			case(op_cnt)
				0: OP = AND;
				1: OP = OR;
				2: OP = ADD;
				3: OP = SUB;
			endcase
			
			A = 32'hffffffff;
			B = 32'hffffffff;
			CRC = CRC4_D68({B, A, 1'b0, OP}, 4'h0);

			#100;
			for(bit_counter = 0; bit_counter <= 10*11; bit_counter = bit_counter + 1)  begin
				send_calculation_data(A, B, OP, CRC, bit_counter, sin);	
			end
			sin = 1;
			calculate_outputs(A,B,OP,CRC,CTL_tb,C_tb);
			
			#2500 display_test_results(A,B,OP,CRC,C_tb,C,CTL_tb,CTL);
			
			compare_results(C, C_tb, CTL, CTL_tb);
			$display("\nEND TEST\n\n\n");
			#500;
		end
//---------------RANDOM-------------------------------------------------------------------------------
		
		for(cnt = 0; cnt < 1000; cnt = cnt + 1) begin
		$display("TEST RANDOM");	
		$display("START TEST\n");
			op_cnt = 'd4%$random;
			case(op_cnt)
				0: OP = AND;
				1: OP = OR;
				2: OP = ADD;
				3: OP = SUB;
			endcase
			
			A = 32'hffffffff%$random;
			B = 32'hffffffff%$random;
			CRC = CRC4_D68({B, A, 1'b0, OP}, 4'h0);

			#100;
			for(bit_counter = 0; bit_counter <= 10*11; bit_counter = bit_counter + 1)  begin
				send_calculation_data(A, B, OP, CRC, bit_counter, sin);	
			end
			sin = 1;
			calculate_outputs(A,B,OP,CRC,CTL_tb,C_tb);
			
			#2500 display_test_results(A,B,OP,CRC,C_tb,C,CTL_tb,CTL);
			
			compare_results(C, C_tb, CTL, CTL_tb);
			#500;
			$display("\nEND TEST\n\n\n");
		end
		

		$display("\n\nTEST RESULTS:\nDONE: %d\nPASSED: %d\nFAILED: %d", passed_tests+failed_tests, passed_tests, failed_tests);
		$finish;
	end

endmodule

