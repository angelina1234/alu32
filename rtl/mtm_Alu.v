/******************************************************************************
 * (C) Copyright 2019 AGH UST All Rights Reserved
 *
 * MODULE:    mtm_Alu
 * PROJECT:   PPCU_VLSI
 * AUTHORS:   Karolina Bocian
 * DATE:	  05.09.2019
 * ------------------------------------------------------------------------------
 * The ALU should operate as described in the mtmAlu_test_top module.
 * It should consist of three modules connected together:
 *   mtm_Alu_deserializer
 *   mtm_Alu_core
 *   mtm_Alu_serializer
 * The ALU should use posedge active clock and synchronous reset active LOW.
 *
 *******************************************************************************/

module mtm_Alu (
    input  wire clk,   // posedge active clock
    input  wire rst_n, // synchronous reset active low
    input  wire sin,   // serial data input
    output wire sout   // serial data output
);

wire [8:0] data_out, data_out2;
wire data_ready, output_ready;


mtm_Alu_deserializer u_mtm_Alu_deserializer(
    .clk(clk),
    .rst(rst_n),
    .sin(sin),
    .data_out(data_out),
    .data_ready(data_ready)
);

mtm_Alu_core u_mtm_Alu_core(
	.clk(clk),
	.rst(rst_n),
	.data_ready(data_ready),
	.data_in(data_out),
	.data_out(data_out2),
	.output_ready(output_ready)
);

mtm_Alu_serializer u_mtm_Alu_serializer(
    	.clk(clk),
    	.rst_n(rst_n), 
    	.data_in(data_out2),
    	.data_ready(output_ready),

    	.sout(sout)
);
 

endmodule
