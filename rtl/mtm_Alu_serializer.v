/******************************************************************************
 * (C) Copyright 2019 AGH UST All Rights Reserved
 *
 * MODULE:    mtm_Alu_serializer
 * PROJECT:   PPCU_VLSI
 * AUTHOR 1:  Karolina Bocian
 * DATE:      05.09.2019
 *
 *******************************************************************************/
 
module mtm_Alu_serializer(
    input wire clk,
    input wire rst_n, 
    input wire [8:0] data_in,
    input wire data_ready,

    output reg sout
    );
    
    localparam
    STATE_BITS		= 2,
    
    STATE_IDLE		= 2'b00,
    STATE_RECEIVE 	= 2'b01,
    STATE_SEND  	= 2'b10;
    
    reg [STATE_BITS-1:0] state, state_nxt;
     
    reg [2:0] byte_counter, byte_counter_nxt;
    reg sout_nxt;
	reg [54:0] data, data_nxt;
	reg [3:0] bit_counter, bit_counter_nxt;

	
    always @(posedge clk) begin
        if (!rst_n) begin
            bit_counter		<= 4'b0;
			byte_counter	<= 3'b0;
            state			<= STATE_IDLE;
            sout			<= 1'b1; 
            data			<= 55'hFFFFFFF;
        end
        else begin
            state			<= state_nxt;
			bit_counter		<= bit_counter_nxt;
            byte_counter	<= byte_counter_nxt;
            data			<= data_nxt;
            sout			<= sout_nxt;
        end
    end
    
    always @*
        begin
            case(state) 
                STATE_IDLE: 	state_nxt = data_ready ? STATE_RECEIVE : STATE_IDLE;
                STATE_RECEIVE: 	state_nxt = (byte_counter <3 && data_ready) ? STATE_RECEIVE : STATE_SEND;
                STATE_SEND: 	state_nxt = (bit_counter == 11 && byte_counter == 0) ? STATE_IDLE :STATE_SEND;
                default : 		state_nxt = STATE_IDLE;
            endcase
        end
        
     always @* begin
        sout_nxt = sout;
        data_nxt = data;
        byte_counter_nxt = byte_counter;
		bit_counter_nxt  = bit_counter;
        case(state)
            STATE_IDLE:		begin
								byte_counter_nxt = 0;
								bit_counter_nxt = 0;
								data_nxt = {data[43:0],1'b0, data_in, 1'b1};
								sout_nxt=1;
                               
							end
            STATE_RECEIVE:  begin
								sout_nxt=1; 
								if (byte_counter == 0 && data[9])
									data_nxt = {data[10:0], data[54:11]};
								else begin 
									data_nxt = {data[43:0],1'b0, data_in, 1'b1};
									byte_counter_nxt = byte_counter + 1;
								end
                            end
            STATE_SEND:     begin
								sout_nxt = data[54 - bit_counter];
								if (bit_counter == 11) begin
									
									if (byte_counter == 0 ) begin
										sout_nxt = 1;
										bit_counter_nxt = 0;
									end
									else begin
										bit_counter_nxt = 1;
										data_nxt = {data[43:0],11'hFFF};
										byte_counter_nxt = byte_counter - 1;
									end
								end
								else bit_counter_nxt = bit_counter + 1 ;
                            end
        endcase
        end

endmodule
