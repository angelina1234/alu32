/******************************************************************************
 * (C) Copyright 2019 AGH UST All Rights Reserved
 *
 * MODULE:    mtm_Alu_deserializer
 * PROJECT:   PPCU_VLSI
 * AUTHOR 1:  Karolina Bocian
 * DATE:      05.09.2019
 *
 *******************************************************************************/
 
module mtm_Alu_deserializer(
    input wire clk,
    input wire rst, 
    input wire sin,
    
    output reg [8:0] data_out,
    output reg data_ready
    );
    
    localparam
    STATE_BITS = 2,
    
    STATE_IDLE    = 2'b00,
    STATE_RECEIVE = 2'b01,
    STATE_SEND  = 2'b10;
    
    reg [STATE_BITS-1:0] state, state_nxt;
     
    reg [3:0] bit_counter, bit_counter_nxt;
    reg [8:0] data_out_nxt;
    reg [8:0] data,data_nxt;
    reg data_ready_nxt;

    always @(posedge clk) begin
        if (!rst) begin
            bit_counter	<= 4'b0;
            state		<= STATE_IDLE;
            data_out	<= 9'b0; 
            data		<= 9'b0;
            data_ready	<= 1'b0;
        end
        else begin
            state		<= state_nxt;
            bit_counter	<= bit_counter_nxt;
            data_out	<= data_out_nxt;
            data		<= data_nxt;
            data_ready	<= data_ready_nxt;
        end
    end
    
    always @*
        begin
            case(state) 
                STATE_IDLE: 	state_nxt = sin ? STATE_IDLE : STATE_RECEIVE;
                STATE_RECEIVE: 	state_nxt = (bit_counter >= 8) ? STATE_SEND : STATE_RECEIVE;
                STATE_SEND: 	state_nxt = STATE_IDLE;
                default :		state_nxt = STATE_IDLE;
            endcase
        end
        
     always @* begin
        data_nxt = data;
        data_out_nxt = data_out;
        bit_counter_nxt = bit_counter;
        data_ready_nxt = data_ready;
        case(state)
            STATE_IDLE:		begin
                       			bit_counter_nxt = 4'b0;
                        		data_nxt = {data[7:0],sin};
                        		data_ready_nxt = 1'b0;
                      		end
            STATE_RECEIVE:	begin
                                bit_counter_nxt = bit_counter + 1;
								data_nxt[8-bit_counter] = sin;
                                data_ready_nxt = 1'b0;
                            end
            STATE_SEND:     begin
                           		data_ready_nxt = sin;
                            	bit_counter_nxt = 4'b0;
                        		data_out_nxt = data[8:0];
                            end
        endcase
        end

endmodule

