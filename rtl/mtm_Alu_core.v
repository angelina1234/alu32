/******************************************************************************
 * (C) Copyright 2019 AGH UST All Rights Reserved
 *
 * MODULE:    mtm_Alu_core
 * PROJECT:   PPCU_VLSI
 * AUTHOR 1:  Karolina Bocian
 * DATE:      05.09.2019
 *
 *******************************************************************************/
 
module mtm_Alu_core(
    input wire clk,
    input wire rst, 
    input wire data_ready,
    input wire [8:0] data_in ,
    
    output reg [8:0] data_out,
    output reg output_ready
    );
    
    localparam
    STATE_BITS = 2,
    
    STATE_IDLE    = 2'b00,
    STATE_RECEIVE = 2'b01,
    STATE_SEND = 2'b10,
    STATE_PROCESS  = 2'b11,

    AND = 3'b000,
    OR  = 3'b001,
    ADD = 3'b100,
    SUB = 3'b101,

    PARITY = 1'b1; // number of ones in {1'b1, ERR_FLAGS} is always odd
    
    reg [STATE_BITS-1:0] state, state_nxt;
     
    reg [31:0] A, B, C, A_nxt, B_nxt, C_nxt;
    reg [2:0] OP, OP_nxt;
    reg [3:0] crc_r, crc_r_nxt;
    reg [2:0] crc_s, crc_s_nxt;
    reg ERR_CRC, ERR_OP, ERR_DATA, Carry, Overflow, Zero, Negative,output_ready_nxt, ERR_CRC_nxt, ERR_OP_nxt, ERR_DATA_nxt, Carry_nxt, Overflow_nxt, Zero_nxt, Negative_nxt;
    reg [3:0] byte_counter_rec, byte_counter_rec_nxt;
    reg [2:0] byte_counter_send, byte_counter_send_nxt;
    reg [8:0] data_out_nxt;

    always @(posedge clk) begin
        if (!rst) begin
            byte_counter_rec   	<= 4'b0;
            state           	<= STATE_IDLE;
            data_out        	<= 9'b0;
            byte_counter_send	<= 3'b0;
   	        A      				<= 32'b0;
            B      				<= 32'b0;
            C      				<= 32'b0;
	    	output_ready		<= 1'b0;
			crc_r				<= 4'b0;
			crc_s				<= 3'b0;
			ERR_CRC				<= 1'b0;
			ERR_OP 				<= 1'b0;
			ERR_DATA 			<= 1'b0;
			OP					<= 3'b0;
			Carry 				<= 1'b0;
			Overflow 			<= 1'b0;
			Zero 				<= 1'b0;	
			Negative 			<= 1'b0;
			
        end
        else begin
            state           	<= state_nxt;
            byte_counter_rec   	<= byte_counter_rec_nxt;
            byte_counter_send   <= byte_counter_send_nxt;
            data_out        	<= data_out_nxt;
	    	output_ready		<= output_ready_nxt;
			crc_r				<= crc_r_nxt;
			crc_s				<= crc_s_nxt;
			A					<= A_nxt;
			B					<= B_nxt;
			C					<= C_nxt;
			OP					<= OP_nxt;
			ERR_CRC				<= ERR_CRC_nxt;
			ERR_OP				<= ERR_OP_nxt;
			ERR_DATA			<= ERR_DATA_nxt;
			Carry				<= Carry_nxt;
			Overflow			<= Overflow_nxt;
			Zero				<= Zero_nxt;
			Negative			<= Negative_nxt;
        end
    end
    
    always @*
        begin
            case(state) 
                STATE_IDLE: state_nxt = data_ready ? STATE_RECEIVE : STATE_IDLE;
                STATE_RECEIVE: begin
					if (ERR_DATA_nxt || ERR_CRC_nxt || ERR_OP_nxt) state_nxt = STATE_SEND;
					else state_nxt = (byte_counter_rec < 8) ? STATE_IDLE : STATE_PROCESS;
				end
				STATE_PROCESS: state_nxt = STATE_SEND;
                STATE_SEND: state_nxt = ((byte_counter_send >= 5) || (ERR_DATA || ERR_CRC || ERR_OP))? STATE_IDLE : STATE_SEND;
            endcase
        end
        
     always @* begin

        data_out_nxt = data_out;
        byte_counter_rec_nxt = byte_counter_rec;
        byte_counter_send_nxt = byte_counter_send;
		output_ready_nxt = output_ready;
		crc_r_nxt = crc_r;
		crc_s_nxt = crc_s;
		A_nxt = A;
		B_nxt = B;
		C_nxt = C;
		ERR_CRC_nxt = ERR_CRC;
		ERR_OP_nxt = ERR_OP;
		ERR_DATA_nxt = ERR_DATA;
		Carry_nxt = Carry;
		Overflow_nxt = Overflow;
		Zero_nxt = Zero;
		Negative_nxt = Negative;
		OP_nxt = OP;

	case(state)
            STATE_IDLE:		begin
                        		byte_counter_send_nxt = 0;
								output_ready_nxt = 0;
								ERR_CRC_nxt = 0;
								ERR_DATA_nxt = 0;
								ERR_OP_nxt = 0;
								Carry_nxt = 0;
								Overflow_nxt = 0;
								Zero_nxt = 0;
								Negative_nxt = 0;
								crc_s_nxt = 0;
								
                       		end
            STATE_RECEIVE:	begin
                                byte_counter_rec_nxt = byte_counter_rec + 1;
                                if (byte_counter_rec <4 && data_in[8] == 0) begin
									B_nxt = {B[23:0], data_in[7:0]};

 									crc_r_nxt[0] = data_in[7] ^ data_in[5] ^ data_in[3] ^ data_in[2] ^ data_in[1] ^ data_in[0] ^ crc_r[1] ^ crc_r[3];
    								crc_r_nxt[1] = data_in[6] ^ data_in[4] ^ data_in[3] ^ data_in[2] ^ data_in[1] ^ crc_r[0] ^ crc_r[2];
   									crc_r_nxt[2] = data_in[7] ^ data_in[5] ^ data_in[4] ^ data_in[3] ^ data_in[2] ^ crc_r[0] ^ crc_r[1] ^ crc_r[3];
    								crc_r_nxt[3] = data_in[7] ^ data_in[6] ^ data_in[4] ^ data_in[2] ^ data_in[1] ^ data_in[0] ^ crc_r[0] ^ crc_r[2] ^ crc_r[3];

								end
								else if (byte_counter_rec <8 && data_in[8] == 0) begin
									A_nxt = {A[23:0], data_in[7:0]};
									crc_r_nxt[0] = data_in[7] ^ data_in[5] ^ data_in[3] ^ data_in[2] ^ data_in[1] ^ data_in[0] ^ crc_r[1] ^ crc_r[3];
    								crc_r_nxt[1] = data_in[6] ^ data_in[4] ^ data_in[3] ^ data_in[2] ^ data_in[1] ^ crc_r[0] ^ crc_r[2];
   									crc_r_nxt[2] = data_in[7] ^ data_in[5] ^ data_in[4] ^ data_in[3] ^ data_in[2] ^ crc_r[0] ^ crc_r[1] ^ crc_r[3];
    								crc_r_nxt[3] = data_in[7] ^ data_in[6] ^ data_in[4] ^ data_in[2] ^ data_in[1] ^ data_in[0] ^ crc_r[0] ^ crc_r[2] ^ crc_r[3];


								end
								else if (byte_counter_rec == 8 && data_in[8] == 1) begin
									OP_nxt = data_in[6:4];
									crc_r_nxt[0] = data_in[7] ^ data_in[5] ^ data_in[3] ^ data_in[2] ^ data_in[1] ^ data_in[0] ^ crc_r[1] ^ crc_r[3];
    								crc_r_nxt[1] = data_in[6] ^ data_in[4] ^ data_in[3] ^ data_in[2] ^ data_in[1] ^ crc_r[0] ^ crc_r[2];
   									crc_r_nxt[2] = data_in[7] ^ data_in[5] ^ data_in[4] ^ data_in[3] ^ data_in[2] ^ crc_r[0] ^ crc_r[1] ^ crc_r[3];
    								crc_r_nxt[3] = data_in[7] ^ data_in[6] ^ data_in[4] ^ data_in[2] ^ data_in[1] ^ data_in[0] ^ crc_r[0] ^ crc_r[2] ^ crc_r[3];


									if (crc_r_nxt) ERR_CRC_nxt = 1;
									if (OP_nxt[1] == 1) ERR_OP_nxt =1;
								end
								else ERR_DATA_nxt = 1;

                         	end
            STATE_PROCESS:	begin
                          		case (OP)
									AND: C_nxt = A & B;
									OR: C_nxt = A | B;
									ADD: begin
											C_nxt = A + B;
											Carry_nxt = ((C_nxt < A) || (C_nxt < B));
											Overflow_nxt = (!(A[31]^B[31]) && (A[31]^C_nxt[31]));
										end
									SUB: begin
											C_nxt = A - B;
											Carry_nxt = (A < C_nxt);
											Overflow_nxt = (!(A[31]^C_nxt[31]) && (B[31]^C_nxt[31]));
										end
									default : C_nxt = 0;
                            	endcase
								Zero_nxt = (C_nxt == 0);
								Negative_nxt = C_nxt[31];
                            end
            STATE_SEND:     begin
                                byte_counter_rec_nxt = 0;
								byte_counter_send_nxt = byte_counter_send + 1; 
								if (!(ERR_CRC || ERR_OP || ERR_DATA)) begin
									if (byte_counter_send <4) begin
											
										crc_s_nxt[0] = C[31] ^ C[28] ^ C[26] ^ C[25] ^ C[24] ^ crc_s[2];
   										crc_s_nxt[1] = C[29] ^ C[27] ^ C[26] ^ C[25] ^ crc_s[0];
    									crc_s_nxt[2] = C[31] ^ C[30] ^ C[27] ^ C[25] ^ C[24] ^ crc_s[1] ^ crc_s[2];

										data_out_nxt = {1'b0, C[31:24]};
										C_nxt = {C[23:0], 8'b00000000};
									end
										

									else begin
									
    									crc_s_nxt[0] = 0 ^ Overflow ^ Zero ^ Negative ^ crc_s[0] ^ crc_s[2];
   										crc_s_nxt[1] = Carry ^ Overflow ^ Zero ^ crc_s[0] ^ crc_s[1];
    									crc_s_nxt[2] = Carry ^ Zero ^ Negative ^ crc_s[1];
										data_out_nxt = {1'b1, 1'b0, Carry, Overflow, Zero, Negative, crc_s_nxt};
									end
								end
								else begin
									data_out_nxt = {1'b1, 1'b1, ERR_DATA, ERR_CRC, ERR_OP, ERR_DATA, ERR_CRC, ERR_OP, PARITY};
									crc_r_nxt = 4'b0;
								end
								output_ready_nxt = 1;
                            end
        endcase
        end

endmodule
